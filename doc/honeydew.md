# Honeydew

## Boss battle

Periodically boss channells a spell for 3 seconds. When it finishes, a doom
guard appears in a random place on the arena. Players should interrupt the cast
with spells like Storm Bolt or Hex, to prevent the appearence of the doom
guard.

When at half health or less, boss will attempt to sacrifice a doom guard with
death pact.

Periodically, about every half a minute since the fight starts, the boss will
make infernals fall from the sky. Infernals destroy nearby trees adding to the
spectacle.

Maybe death knights (revenant creature, not hero) periodically appear and cast
death coil on the boss to heal it.

Felhunters (mage hunters) appear from portals and mana burn player heroes.

## Boss arena

The arena in which boss battle takes place is a lake or a river during early
spring or early winter.

Maybe make it so water leads to the hero's base.

The arena is isolated and must be teleported to. To activate the teleporter
waygate, three keys must be obtained.

## Player gold gain

Players gain gold by killing lesser minions of the demon. Players may spend
gold to buy consumable items, but probably not permanent upgrades. Permanent
upgrades should probably be obtainable from killing specific mini bosses.

Hero revival costs gold, approximately the same amount as it would cost to use
altars in melee mode. When all heroes are dead and there is not enough gold to
purchase a revival, users are considered defeated.

When any hero kills any enemy monster, all users gain equal amount of gold from
the kill. This avoids kill stealing. This also allows different users to play
separately and still benefit from each other indirectly. Maybe do the same for
experience, but it's more tricky and probably not worth it. Respawning enemies
should fix any level gaps.
