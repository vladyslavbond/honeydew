# honeydew build instructions

## Motivation

The purpose of the custom build process is to aid script developer convenience
and add some advanced development features. Specifically, macro preprocessor,
dependency management, detailed error messages, automated packaging,
versioning. Also planned for eventual implementation are globalization and
semi-automated testing.

Most of the major features were implemented in the past by Word Editor
modifications, like Jass New Gen Pack or vjass. Instead of implementing a
custom integrated solution, this project relies much more on free generic
software development tools.

## Dependencies

### Windows

To build this project on Windows advanced user knowledge is required.
Contributions to the instructions are welcome. Obviously, the resulting map
archive file must work in game regardless of the underlying operating system.

Most likely, at least Windows `Git Bash` will be required. First to install
`git`, second to install UNIX-like shell, that is required for `make` to work.

`make` and `m4` should be easy to install after that, as they are popular
tools.

`smpq` is much more obscure. It may require some more effort to install.

`smpq` may be effectively replaced by any of the MPQ editors for Windows that
support command-line arguments. The problem then is in patching the project's
"`Makefile`" to employ different command argument order. Users are on their own
here.

### Debian 12 (bullseye)

```
apt-get install git m4 make smpq
```

Most importantly, it requires `pjass`. Some distributions of the game may bundle
`pjass`.

Upstream `pjass` repository may be found here:

https://github.com/lep/pjass

Fork of `pjass` with Debian 12 packaging may be found here:

https://rocketgit.com/user/vrtc/pjass

## Build

### Build summary

After the map is saved in the World Editor, the World Editor will remove all
the patches this project makes to the map archive file. Therefore, the map will
become unplayable.

To make the scripts work, this build process must be followed. Every time
updates to terrain or objects need to be saved, the build process for scripts
must be repeated again, to make sure the map archive file is patched correctly.

The only supported version of the game is 1.27. It may work on some slightly
older or slightly newer versions.

In an ideal world, simply executing the "`Makefile`" should produce a valid map
archive file. The instructions assume that the current directory is the
project's root, that is base directory, that is sources directory ("`srcdir`").

```
make
```

### Build details

The following make tasks should help troubleshooting. Generally, the project
aims to to follow GNU `make` convetions whenever possible.

Ensure that the build directory exists. By default, the build directory is set
to "target".

```
mkdir -p target/
```

The build directory may be overriden like this, for every `make` invocation.

```
make BUILDDIR=/home/myuser/mybuilddir/
```

Similarly, the project base directory, or sources directory, may be overriden
like this. Then the "`Makefile`" location must be explicitly specified as well.
Overriding build and sources directory may be convenient for setting up
development environment or packaging.

```
make -f /home/myuser/mysrcdir/Makefile srcdir=/home/myuser/mysrcdir/
```

Check Jass script snippets for validity. This does not necessarily require the
game installed, only `pjass`, `m4` and `make`.

```
make check
```

Build playable map archive file. This requires `pjass`, `m4`, `make` and
`smpq`.

```
make all
```

Assemble a distribution zip package, that contains the playable map and all the
sources needed to rebuild it. This requires all of the above and `git`.

```
make dist
```

Test the map in the game.

```
make run
```

This requires the game to be installed. Additionally, this requires the
developer to create a local "`Makefile.config`" that is not commited to the
project's repository. Makefile.config must be placed in the same directory as
the "`Makefile`" (normally under `srcdir`).

Example "`Makefile.config`".

```
WAR3EXE=wine64 ~/local/share/wine-devel/drive_c/games/warcraft3-1.27/Frozen\ Throne.exe -opengl -window
```

Build and run the map with development stage test scripts.

```
make TESTS=test/test.j run
```

The Jass snippet "`test/test.j`" must exist. The snippet must define "`function
test_init`", that is called from "`src/main.j:function main`".
