NAME=honeydew
VERSION=0.1.1-WIP
CLASSIFIER=1.27
LANG=en

NAMEFULL=$(NAME)-$(VERSION)-$(CLASSIFIER)-$(LANG)
NAMEASSEMBLY=$(NAME)-$(VERSION)-project

SHELL=/bin/sh
.SUFFIXES:

BUILDDIR=target
srcdir=.

.PRECIOUS: *.w3x *.w3m *.w3n
.PHONY: clean distclean dist run check all $(BUILDDIR)/$(NAMEASSEMBLY).zip

GIT=git
GPG=gpg
SMPQ=smpq

M4=m4
M4FLAGS=

PJASS=pjass
PJASSFLAGS="$(srcdir)/etc/common.j" "$(srcdir)/etc/Blizzard.j" -rb +filter +shadow +checkglobalsinit +checkstringhash
SNIPPETS=$(wildcard $(srcdir)/src/*.j)

WAR3EXE=Frozen\ Throne.exe
# TODO add configurable map path for testing
MAPSDIR="Maps/$(NAME)/"

#vpath *.j src:$(BUILDDIR)

include $(srcdir)/src/Makefile
sinclude $(srcdir)/Makefile.config
sinclude $(srcdir)/test/Makefile

all: $(BUILDDIR)/$(NAMEFULL).w3x $(BUILDDIR)/war3map.j | $(BUILDDIR)
	$(PJASS) $(PJASSFLAGS) "$(BUILDDIR)/war3map.j"
	cd "$(BUILDDIR)" && \
	$(SMPQ) --append --overwrite "$(NAMEFULL).w3x" "war3map.j" && \
	$(SMPQ) --list "$(NAMEFULL).w3x"

dist: $(BUILDDIR)/$(NAMEASSEMBLY).zip $(BUILDDIR)/$(NAMEASSEMBLY).zip.sig | $(BUILDDIR)

install: all
	cp "$(BUILDDIR)/$(NAMEFULL).w3x" "$(DESTDIR)"

$(BUILDDIR)/$(NAMEFULL).w3x: $(srcdir)/$(NAME).w3x | $(BUILDDIR)
	cp "$(srcdir)/$(NAME).w3x" "$(BUILDDIR)/$(NAMEFULL).w3x"

$(BUILDDIR):
	mkdir "$(BUILDDIR)"

check: $(SNIPPETS)
	$(PJASS) $(PJASSFLAGS) $(SNIPPETS) $(TESTS)

$(BUILDDIR)/war3map.j: $(srcdir)/src/jass_include.m4 $(SNIPPETS) | $(BUILDDIR) check
	$(M4) --debug=aeqp -I $(srcdir) $(M4FLAGS) $(srcdir)/src/jass_include.m4 $(SNIPPETS) $(TESTS) > "$(BUILDDIR)/war3map.j"

$(BUILDDIR)/$(NAMEASSEMBLY).zip: | $(BUILDDIR)
	$(GIT) archive --format zip --output "$(BUILDDIR)/$(NAMEASSEMBLY).zip" --prefix "$(NAME)-$(VERSION)/" HEAD

$(BUILDDIR)/$(NAMEASSEMBLY).zip.sig: $(BUILDDIR)/$(NAMEASSEMBLY).zip
	$(GPG) --detach-sign --yes "$(BUILDDIR)/$(NAMEASSEMBLY).zip"

clean:
	$(GIT) clean -i -x "$(BUILDDIR)"

distclean:
	$(GIT) clean -i -x "$(srcdir)"

run: all
	# Path to the map relative to data directory (game installation directory in almost every case).
	# Must be given exact file name. Wildcards or tokens will not be expanded.
	$(WAR3EXE) -loadfile "Maps/${NAME}/${NAMEFULL}.w3x"
