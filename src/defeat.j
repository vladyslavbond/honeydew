// jass_module(`SRC_DEFEAT_J')
// jass_include(`SRC_USER_J', `src/user.j')
// jass_include(`SRC_REVIVE_J', `src/revive.j')

globals

	// src/defeat.j fields

endglobals

// src/defeat.j functions

function defeat_trig_filter takes nothing returns boolean
	local unit u = GetFilterUnit()
	local player p = null
	local boolean flag = true

	if null == u then
		return false
	endif

	set flag = IsHeroUnitId(GetUnitTypeId(u)) and flag
	set flag = IsUnitType(u, UNIT_TYPE_HERO) and flag
	set p = GetOwningPlayer(u)
	set flag = user_player_is_user_check(p) and flag
	return flag
endfunction

// Users are considered defeated when there are no units left alive,
// and no gold left to revive dead heroes,
// and no revivals are ongoing, across all user force.
function defeat_check takes nothing returns boolean
	local integer revive_price_min = REVIVE_PRICE_BASE
	local integer q = 0
	local integer i = 0
	local integer g = 0
	local player p = null
	local integer y = 0
	local boolean flag = false

	if revive_ongoing_check() then
		return false
	endif

	set i = 0
	loop
		exitwhen i >= bj_MAX_PLAYERS
		set p = Player(i)
		if user_player_is_user_check(p) then
			set y = y + 1
			set q = GetPlayerUnitCount(p, true) + q
			set g = GetPlayerState(p, PLAYER_STATE_RESOURCE_GOLD) + g
		endif
		set i = i + 1
	endloop

	return y > 0 and 0 == q and g < revive_price_min
endfunction

function defeat_dialog_force_callback takes nothing returns nothing
	local player p = GetEnumPlayer()
	local string fluff = "All heroes are dead and cannot afford to revive."
	call CustomDefeatBJ(p, fluff)
endfunction

// Force defeat dialog on all users.
function defeat_dialog takes nothing returns nothing
	local force f = user_force_get()
	call ForForce(f, function defeat_dialog_force_callback)
endfunction

function defeat_timer_callback takes nothing returns nothing
	local timer t = null

	set t = GetExpiredTimer()
	call DestroyTimer(t)

	call defeat_dialog()
endfunction

// Show defeat message to all users.
function defeat_quest_msg takes nothing returns nothing
	local string fluff = null
	local force f = user_force_get()

	set fluff = "|cffffcc00MISSION FAILED|r\n"
	set fluff = fluff + "All heroes are dead and cannot afford to revive"
	call QuestMessageBJ(f, bj_QUESTMESSAGE_MISSIONFAILED, fluff)
endfunction

// End the game with defeat for all users.
// Show quest message, delay, then show defeat dialog.
function defeat takes nothing returns nothing
	local timer t = null
	call defeat_quest_msg()
	set t = CreateTimer()
	call TimerStart(t, bj_QUEUE_DELAY_QUEST, false, function defeat_timer_callback)
endfunction

function defeat_trig_action takes nothing returns nothing
	local trigger t = null

	if defeat_check() then
		set t = GetTriggeringTrigger()
		call DisableTrigger(t)
		call defeat()
	endif
endfunction

function defeat_fluff_init takes nothing returns defeatcondition
	local defeatcondition d = null
	set d = CreateDefeatCondition()
	// FIXME globalize defeat condition fluff
	call DefeatConditionSetDescription(d, "demons win when all heroes are dead and cannot afford to revive")
	return d
endfunction

function defeat_init takes nothing returns trigger
	local trigger t = null
	local integer i = 0
	local boolexpr filter = null

	set t = CreateTrigger()

	set filter = Condition(function defeat_trig_filter)

	loop
		exitwhen i >= bj_MAX_PLAYERS
		call TriggerRegisterPlayerUnitEvent(t, Player(i), EVENT_PLAYER_UNIT_DEATH, null)
		set i = i + 1
	endloop
	call TriggerAddAction(t, function defeat_trig_action)

	call defeat_fluff_init()

	return t
endfunction
