// jass_module(SRC_USER_J)

// TODO Allow computer players to be allocated starting units, then transfer
// advanced control to live users.

globals

	// src/user.j fields

	// Unit groups or player forces are mutable collections.
	// They may never be effectively constant.
	force USER_FORCE = null
endglobals

// src/user.j functions

// Check if given player is an active user and not computer or left.
function user_player_is_user_check takes player p returns boolean
	local boolean flag = true
	if null == p then
		return false
	endif
	set flag = MAP_CONTROL_USER == GetPlayerController(p) and flag
	set flag = PLAYER_SLOT_STATE_PLAYING == GetPlayerSlotState(p) and flag
	set flag = not IsPlayerObserver(p) and flag
	return flag
endfunction

function user_force_filter takes nothing returns boolean
	local player p = GetFilterPlayer()
	return user_player_is_user_check(p)
endfunction

function user_force_enum takes force user_force returns force
	local filterfunc filter = null
	if null == user_force then
		return null
	endif
	set filter = Filter(function user_force_filter)
	call ForceEnumPlayers(user_force, filter)
	call DestroyFilter(filter)
	return user_force
endfunction

function user_force_get takes nothing returns force
	local force f = null

	if null == USER_FORCE then
		set USER_FORCE = CreateForce()
	endif	

	set f = USER_FORCE
	call ForceClear(f)
	call user_force_enum(f)
	if null == f then
		call BJDebugMsg("src/user.j: invalid state: force object missing")
	endif
	return f
endfunction

function user_init_player takes player p returns nothing
	local location loc = null
	local integer start = 0

	call SetPlayerTechMaxAllowed(p, 'HERO', 0)
	call SetPlayerState(p, PLAYER_STATE_RESOURCE_FOOD_CAP, 40)
	call SetPlayerState(p, PLAYER_STATE_RESOURCE_GOLD, 0)
	call SetPlayerState(p, PLAYER_STATE_RESOURCE_LUMBER, 0)

	set start = GetPlayerStartLocation(p)
	set loc = GetStartLocationLoc(start)

	call RemoveLocation(loc)
endfunction

function user_init_player_callback takes nothing returns nothing
	local player p = GetEnumPlayer()
	call user_init_player(p)
endfunction

// Blizzard.j:function CountPlayersInForceBJ serves the same purpose.
// The benefit of this alternative implementation is that is avoids mutable global variables.
function user_force_count takes force anyforce returns integer
	local integer i = 0
	local player p = null
	local integer q = 0

	if null == anyforce then
		return -1
	endif

	set i = 0
	loop
		exitwhen i >= bj_MAX_PLAYER_SLOTS
		set p = Player(i)
		if IsPlayerInForce(p, anyforce) then
			set q = q + 1
		endif
		set i = i + 1
	endloop

	return q
endfunction

function user_quantity_get takes nothing returns integer
	local force user_force = null
	local integer q = 0

	set user_force = CreateForce()
	call user_force_enum(user_force)
	set q = user_force_count(user_force)

	call DestroyForce(user_force)
	return q
endfunction

function user_init takes nothing returns nothing
	local force user_force = CreateForce()
	call user_force_enum(user_force)

	call ForForce(user_force, function user_init_player_callback)
	call DestroyForce(user_force)
endfunction
