// jass_module(`SRC_ATUN_J')

globals
	constant integer ATUN_ABILITY_ID = 'A003'

	constant real ATUN_RESTORE_MANA_BASE = 25.0
	constant real ATUN_RESTORE_MANA_COEF = 25.0
	constant real ATUN_RESTORE_MANA_MIN = 0.0
	constant real ATUN_RESTORE_MANA_MAX = 65536.0

	ubersplat array atun_decal
	timer array atun_decal_timer
	effect array atun_decal_effect
	integer atun_decal_head = 0
endglobals

function atun_decal_destroy takes integer i returns nothing
	local integer p = 0
	local integer q = 0
	local integer j = 0

	call DestroyTimer(atun_decal_timer[i])
	call DestroyUbersplat(atun_decal[i])
	call DestroyEffect(atun_decal_effect[i])

	set p = i
	set j = IMinBJ(IMaxBJ(0, atun_decal_head), JASS_MAX_ARRAY_SIZE - 1)
	loop
		exitwhen p >= j or q >= j
		set q = p + 1
		set atun_decal[p] = atun_decal[q]
		set atun_decal_timer[p] = atun_decal_timer[q]
		set atun_decal_effect[p] = atun_decal_effect[q]
		set p = p + 1
	endloop
endfunction

function atun_decal_timer_callback takes nothing returns nothing
	local timer t = GetExpiredTimer()
	local integer i = 0
	local integer j = 0

	set j = IMinBJ(IMaxBJ(0, atun_decal_head), JASS_MAX_ARRAY_SIZE - 1)
	loop
		exitwhen i >= j
		if t == atun_decal_timer[i] then
			call atun_decal_destroy(i)
			set atun_decal_head = atun_decal_head - 1
		endif
		set i = i + 1
	endloop
endfunction

function atun_decal_create takes location loc, real duration_sec returns integer
	local timer t = null
	local ubersplat splat = null
	local real x = 0.0
	local real y = 0.0
	local effect e = null

	if null == loc then
		return 0
	endif

	if duration_sec < 1.0 then
		return 0
	endif

	if atun_decal_head >= JASS_MAX_ARRAY_SIZE then
		return 0
	endif

	set x = GetLocationX(loc)
	set y = GetLocationY(loc)
	set splat = CreateUbersplat(x, y, "HCRT", 255, 255, 255, 255, false, false)
	//call FinishUbersplat(splat)
	call ShowUbersplat(splat, true)
	call SetUbersplatRenderAlways(splat, true)

	set t = CreateTimer()
	set atun_decal_timer[atun_decal_head] = t
	set atun_decal[atun_decal_head] = splat

	set e = AddSpellEffectByIdLoc(ATUN_ABILITY_ID, EFFECT_TYPE_SPECIAL, loc)
	set atun_decal_effect[atun_decal_head] = e

	set atun_decal_head = atun_decal_head + 1

	call TimerStart(t, duration_sec, false, function atun_decal_timer_callback)

	return GetHandleId(t)
endfunction

function atun_mana_restore_amount_get takes unit caster, unit victim returns real
	local effect e = null

	local location t = null

	local integer ability_level = 0
	local real mana = 0.0

	if null == caster then
		return 0.0
	endif

	if null == victim then
		return 0.0
	endif

	if GetUnitState(victim, UNIT_STATE_LIFE) > 0.0 then
		return 0.0
	endif

	if not IsUnitType(victim, UNIT_TYPE_DEAD) then
		return 0.0
	endif

	if GetUnitState(victim, UNIT_STATE_MAX_MANA) < 1.0 then
		return 0.0
	endif

	set ability_level = GetUnitAbilityLevel(caster, ATUN_ABILITY_ID)
	if ability_level < 1 then
		return 0.0
	endif

	set mana = ATUN_RESTORE_MANA_BASE + I2R(ability_level) * ATUN_RESTORE_MANA_COEF
	set mana = RMinBJ(RMaxBJ(ATUN_RESTORE_MANA_MIN, mana), ATUN_RESTORE_MANA_MAX)

	if mana > 0.0 then
		// TODO src/atun.j: add persistent ash pile effect
		// TODO src/atun.j: add floating test showing amount of mana restored
		set t = GetUnitLoc(victim)
		set e  = AddSpellEffectByIdLoc(ATUN_ABILITY_ID, EFFECT_TYPE_TARGET, t)
		call DestroyEffect(e)
		// Assume the victim is dead at this point.
		// Killing and exploding the unit is for graphics only here.
		if not IsUnitType(victim, UNIT_TYPE_HERO) then
			call SetUnitExploded(victim, true)
			call KillUnit(victim)
			call ShowUnit(victim, false)
			call atun_decal_create(t, 12.0)
		endif
		call RemoveLocation(t)
	endif

	return mana
endfunction

function atun_mana_restore takes unit u, real mana returns nothing
	local integer i = 0
	local effect e = null

	local real a = 0.0
	local real b = 0.0

	set a = GetUnitState(u, UNIT_STATE_MANA)
	call SetUnitState(u, UNIT_STATE_MANA, a + mana)
	set b = GetUnitState(u, UNIT_STATE_MANA)

	if b > a then
		set i = ATUN_ABILITY_ID
		set e  = AddSpellEffectTargetById(i, EFFECT_TYPE_CASTER, u, "origin")
		call DestroyEffect(e)
	endif
endfunction
