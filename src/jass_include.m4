dnl This macro script should be used to arrange arbitrary number of Jass script
dnl snippets into a single file (war3map.j), that will be interpreted by the
dnl game environment at runtime.
dnl
dnl Example usage.
dnl ```
dnl m4 src/jass_include.m4 src/*.j > war3map.j
dnl pjass etc/common.j Blizzard.j war3map.j
dnl ```
dnl
dnl Most importantly, this script will ensure that there is only one globals
dnl block. The parsing tool pjass that is used by the community will interpret
dnl multiple globals blocks correctly. The game runtime environment itself will
dnl not.
dnl
dnl Optionally, this script will re-arrange and re-order separate Jass script
dnl snippets, in accordance with their dependencies. For this feature to work,
dnl the snippets themselves must incorporate macros into their source. The
dnl snippet may still be parsed normally with pjass or the game runtime
dnl environment with the macros included. For details, see the definitions of
dnl jass_module and jass_include macros. See src/*.j files in the repository
dnl for example usage of the preprocessor macros in Jass script snippets.
dnl
define(`jass_divert_globals',
	`define(`globals', `divert(0)divert(1)')define(`endglobals', `divert(0)divert(2)')dnl'
)dnl
define(`jass_undivert_globals',
	`undefine(`globals')undefine(`endglobals')dnl'
)dnl
dnl Divert globals to be re-inserted after all input was processed.
jass_divert_globals()dnl
dnl
dnl jass_module macro should be used in the file that defines the module.
dnl A module is a Jass script snippet in a single file that pjass should parse
dnl successfully.
dnl
dnl When Jass module is declared, check if it was already declared previously.
dnl Given the module was already declared, skip the contents of the file,
dnl and stop diverting globals.
dnl Given the module is yet unknown, then declare the module,
dnl include the content normally, and resume to divert globals.
dnl
dnl (Note, divert(-1) prevents tokens from being flushed to the output.
dnl divert(-1) does not prevent tokens from being expanded. In this particular
dnl case, this is a big problem, because diversions of globals declarations and
dnl function definitions. Without undefining the macros that implement the
dnl diversions, the output will have duplicated declarations, the exact issue
dnl this script tries to fix.)
dnl
define(`jass_module',
	`ifdef(`$1',
		`skip `$1' jass_undivert_globals()divert(-1)dnl',
		`module `$1' define($1, 1)jass_divert_globals()dnl'
	)'
)dnl
dnl
dnl jass_include macro should be used to declare dependency of a module,
dnl on distinct another module.
dnl
dnl When Jass dependency is declared, check if the required module
dnl was already included.
dnl Given the module was already declared as included,
dnl then do nothing.
dnl Given the module was not yet included,
dnl then include it and declare it to be included,
dnl to inform the rest of the script that it mustn't be duplicated.
dnl dnl
define(`jass_include',
	`ifdef(`$1',
		`requires $2',
		`insert `$1' include($2)define(`$1', 1)dnl'
	)'
)dnl
dnl
dnl Divert functions to be re-inserted after all input was processed.
divert(2)dnl
dnl
dnl After all input was processed, undivert globals and functions,
dnl to be re-inserted in a valid order.
m4wrap(dnl
`divert(0)undefine(`globals')undefine(`endglobals')dnl
globals
undivert(1)dnl
endglobals
undivert(2)dnl'
)dnl
