// jass_module(`SRC_ALCE_J')
// jass_include(`SRC_SPELL_J', `src/spell.j')
// jass_include(`SRC_ATUN_J', `src/atun.j')

// Spell Arcane Lance.
// Beam!

globals

	// src/alce.j fields

	constant integer ALCE_ABILITY_ID = 'A002'

	constant real ALCE_CELL_RADIUS = 128.0

	constant real ALCE_REACH_BASE = 600.0
	constant real ALCE_REACH_MAX = 65536.0
	constant real ALCE_REACH_MIN = 128.0

	constant real ALCE_DAMAGE_AMOUNT_PER_TIMEOUT = 15.0
	constant real ALCE_DURATION_SEC = 12.0
	constant real ALCE_TIMEOUT_SEC = 0.25

	constant real ALCE_OFFSET_Z = 32.0

	integer array alce_ability_id
	integer array alce_order_id
	unit array alce_caster
	real array alce_damage
	location array alce_finish
	location array alce_start
	timer array alce_timer
	effect array alce_area_effect
	lightning array alce_bolt

	integer alce_head = 0
endglobals

// src/alce.j functions

// On the line AB find the offset point C.
function alce_loc_get takes location a, location b, real offset returns location
	local real a0 = GetLocationX(a)
	local real a1 = GetLocationY(a)
	local real b0 = GetLocationX(b)
	local real b1 = GetLocationY(b)
	local real v0 = b0 - a0
	local real v1 = b1 - a1
	local real vm = SquareRoot(v0 * v0 + v1 * v1)
	local real d0 = 0.0
	local real d1 = 0.0
	if vm < 1.0 then
		return b
	endif
	set d0 = v0 / vm
	set d1 = v1 / vm
	return Location(a0 + offset * d0, a1 + offset * d1)
endfunction

function alce_destroy takes integer i returns nothing
	local integer j = 0
	local integer p = 0
	local integer q = 0

	if i < 0 or i >= JASS_MAX_ARRAY_SIZE then
		return
	endif

	call RemoveLocation(alce_start[i])
	call RemoveLocation(alce_finish[i])
	call DestroyTimer(alce_timer[i])
	call DestroyEffect(alce_area_effect[i])
	call DestroyLightning(alce_bolt[i])

	set j = IMinBJ(IMaxBJ(0, alce_head), JASS_MAX_ARRAY_SIZE)
	if j < 0 or j >= JASS_MAX_ARRAY_SIZE then
		return
	endif

	set p = i
	loop
		set q = p + 1
		exitwhen i >= j or q >= JASS_MAX_ARRAY_SIZE
		set alce_ability_id[p] = alce_ability_id[q]
		set alce_caster[p] = alce_caster[q]
		set alce_damage[p] = alce_damage[q]
		set alce_finish[p] = alce_finish[q]
		set alce_start[p] = alce_start[q]
		set alce_timer[p] = alce_timer[q]
		set alce_bolt[p] = alce_bolt[q]
		set alce_area_effect[p] = alce_area_effect[q]
		set p = p + 1
	endloop
endfunction

function alce_group_filter takes nothing returns boolean
	local unit u = null
	local boolean flag = true

	set u = GetFilterUnit()

	if null == u then
		return false
	endif

	set flag = GetUnitState(u, UNIT_STATE_LIFE) > 0.0 and flag
	set flag = not IsUnitType(u, UNIT_TYPE_DEAD) and flag
	return flag
endfunction

function alce_unit_find takes location a, location b returns unit
	local filterfunc filter = null
	local group g = null
	local integer i = 0
	local integer j = 0
	local integer p = 0
	local real a0 = 0.0
	local real a1 = 0.0
	local real b0 = 0.0
	local real b1 = 0.0
	local real c0 = 0.0
	local real c1 = 0.0
	local real d0 = 0.0
	local real d1 = 0.0
	local real v0 = 0.0
	local real v1 = 0.0
	local real vm = 0.0
	local real abm = 0.0
	local real afm = 0.0
	local real aum = 0.0
	local real radius = 0.0
	local real offset = 0.0
	local unit f = null
	local unit u = null
	local location c = null
	local location floc = null
	local location uloc = null

	if null == a then
		return null
	endif

	if null == b then
		return null
	endif

	set g = CreateGroup()
	set filter = Filter(function alce_group_filter)

	set radius = ALCE_CELL_RADIUS

	set a0 = GetLocationX(a)
	set a1 = GetLocationY(a)

	set b0 = GetLocationX(b)
	set b1 = GetLocationY(b)

	set offset = 64.0
	set abm = DistanceBetweenPoints(a, b)
	set d0 = (b0 - a0) / abm
	set d1 = (b1 - a1) / abm
	set v0 = offset * d0
	set v1 = offset * d1
	set vm = SquareRoot(v0 * v0 + v1 * v1)

	// Find any valid group of units g on the path from A to B.
	set i = 0
	set j = R2I(abm / vm) + 1
	set c = Location(a0, a1)
	loop
		set f = FirstOfGroup(g)

		exitwhen f != null or i >= j

		call GroupClear(g)
		call GroupEnumUnitsInRangeOfLoc(g, c, radius, filter)

		set c0 = GetLocationX(c)
		set c1 = GetLocationY(c)
		call MoveLocation(c, c0 + v0, c1 + v1)

		set i = i + 1
	endloop

	// Find first unit u closest to location A that group g contains.
	set p = 0
	set u = f
	set uloc = GetUnitLoc(u)
	set aum = DistanceBetweenPoints(a, uloc)
	loop
		set f = FirstOfGroup(g)
		exitwhen null == f or p >= JASS_MAX_ARRAY_SIZE

		set floc = GetUnitLoc(f)
		set afm = DistanceBetweenPoints(a, floc)
		if afm < aum then
			set u = f
			set aum = afm
		endif
		call RemoveLocation(floc)

		call GroupRemoveUnit(g, f)
		set p = p + 1
	endloop

	call DestroyGroup(g)
	call DestroyFilter(filter)
	call RemoveLocation(c)
	call RemoveLocation(uloc)

	return u
endfunction

function alce_apply takes integer ability_id, unit caster, location start, location finish, real damage, lightning bolt returns nothing
	local unit u = null
	local boolean attack = true
	local boolean ranged = true
	local effect e = null
	local real m = 0.0
	local real a0 = 0.0
	local real a1 = 0.0
	local real a2 = 0.0
	local real b0 = 0.0
	local real b1 = 0.0
	local real b2 = 0.0
	local location uloc = null

	if null == start then
		return
	endif

	if null == finish then
		return
	endif

	set u = alce_unit_find(start, finish)

	if bolt != null then
		set a0 = GetLocationX(start)
		set a1 = GetLocationY(start)
		set a2 = GetLocationZ(start) + ALCE_OFFSET_Z
		set b0 = GetLocationX(finish)
		set b1 = GetLocationY(finish)
		set b2 = GetLocationZ(finish) + ALCE_OFFSET_Z
		if u != null then
			set b0 = GetUnitX(u)
			set b1 = GetUnitY(u)
			if GetUnitDefaultFlyHeight(u) > 0.0 then
				set b2 = GetUnitFlyHeight(u)
			else
				set uloc = GetUnitLoc(u)
				set b2 = GetLocationZ(uloc) + ALCE_OFFSET_Z
				call RemoveLocation(uloc)
			endif
		endif
		call MoveLightningEx(bolt, true, a0, a1, a2, b0, b1, b2)
	endif

	if null == u then
		return
	endif

	// TODO find a way for arcane lance to exclude invulnerable units
	// Only deal damage to units that are vulnerable to magic.
	// But still allow untis invulnerable to damage to block the path of the beam.
	if not IsUnitType(u, UNIT_TYPE_MAGIC_IMMUNE) then
		// Note that this damage is affected by victim's armour type.
		call UnitDamageTarget(caster, u, damage, attack, ranged, ATTACK_TYPE_MAGIC, DAMAGE_TYPE_MAGIC, WEAPON_TYPE_WHOKNOWS)

		if caster != null and u != null then
			set m = atun_mana_restore_amount_get(caster, u)
			call atun_mana_restore(caster, m)
		endif
	endif

	set e = AddSpellEffectTargetById(ability_id, EFFECT_TYPE_TARGET, u, "origin")
	call DestroyEffect(e)
endfunction

function alce_timer_callback takes nothing returns nothing
	local integer i = 0
	local integer j = 0
	local integer k = JASS_MAX_ARRAY_SIZE
	local timer t = null
	local unit caster = null
	local boolean caster_alive = false
	local boolean caster_exists = false

	set t = GetExpiredTimer()

	set i = 0
	set j = IMinBJ(IMaxBJ(0, alce_head), JASS_MAX_ARRAY_SIZE)
	set k = JASS_MAX_ARRAY_SIZE
	loop
		exitwhen i >= j or (k >= 0 and k < JASS_MAX_ARRAY_SIZE)
		if t == alce_timer[i] then
			set k = i
		endif
		set i = i + 1
	endloop

	if k < 0 or k >= JASS_MAX_ARRAY_SIZE then
		return
	endif

	set caster = alce_caster[k]

	if alce_order_id[k] != GetUnitCurrentOrder(caster) then
		call alce_destroy(k)
		set alce_head = alce_head - 1
		return
	endif

	// TODO add laser beam

	set caster_exists = caster != null
	set caster_alive = GetUnitState(caster, UNIT_STATE_LIFE) > 0.0
	set caster_alive = caster_alive and not IsUnitType(caster, UNIT_TYPE_DEAD)

	if caster_exists and caster_alive then
		call alce_apply(alce_ability_id[k], alce_caster[k], alce_start[k], alce_finish[k], alce_damage[k], alce_bolt[k])
	else
		call alce_destroy(k)
		set alce_head = alce_head - 1
	endif
endfunction

function alce_create takes integer order_id, integer ability_id, unit caster, location start, location finish, real damage returns nothing
	local integer i = 0
	local location a = null
	local location b = null
	local lightning bolt = null
	local real a0 = 0.0
	local real a1 = 0.0
	local real a2 = 0.0
	local real b0 = 0.0
	local real b1 = 0.0
	local real b2 = 0.0

	if null == start then
		return
	endif

	if null == finish then
		return
	endif

	if alce_head < 0 or alce_head >= JASS_MAX_ARRAY_SIZE then
		return
	endif

	set i = alce_head

	set alce_ability_id[i] = ability_id
	set alce_order_id[i] = order_id

	set alce_caster[i] = caster

	set alce_damage[i] = damage

	set b0 = GetLocationX(finish)
	set b1 = GetLocationY(finish)
	set b2 = GetLocationZ(finish) + ALCE_OFFSET_Z
	set b = Location(b0, b1)
	set alce_finish[i] = b

	set a0 = GetLocationX(start)
	set a1 = GetLocationY(start)
	set a2 = GetLocationZ(start) + ALCE_OFFSET_Z
	set a = Location(a0, a1)
	set alce_start[i] = a

	set bolt = AddLightningEx("FORK", true, a0, a1, a2, b0, b1, b2)
	call SetLightningColor(bolt, 1.0, 1.0, 1.0, 1.0)
	set alce_bolt[i] = bolt

	set alce_area_effect[i] = AddSpellEffectByIdLoc(ability_id, EFFECT_TYPE_AREA_EFFECT, a)

	set alce_timer[i] = CreateTimer()

	set alce_head = alce_head + 1

	// TODO src/alce.j: add gradual mana drain while channeling

	call TimerStart(alce_timer[i], ALCE_TIMEOUT_SEC, true, function alce_timer_callback)
	call alce_apply(alce_ability_id[i], alce_caster[i], alce_start[i], alce_finish[i], alce_damage[i], alce_bolt[i])
endfunction

function alce_trig_filter takes nothing returns boolean
	return ALCE_ABILITY_ID == GetSpellAbilityId()
endfunction

function alce_trig_action takes nothing returns nothing
	local integer ability_id = 0
	local integer order_id = 0
	local unit caster = null
	local location a = null
	local location b = null
	local location c = null
	local location d = null

	set ability_id = GetSpellAbilityId()

	set caster = GetSpellAbilityUnit()

	if null == caster then
		return
	endif

	set d = GetSpellTargetLoc()

	if null == d then
		return
	endif

	set a = GetUnitLoc(caster)
	set b = alce_loc_get(a, d, ALCE_CELL_RADIUS + 32.0)
	set c = alce_loc_get(b, d, ALCE_REACH_BASE)

	// FIXME src/alce.j: caster may aim Arcane Lance at self, bend the beam path weirdly and harm self
	set order_id = GetUnitCurrentOrder(caster)
	call alce_create(order_id, ability_id, caster, b, c, ALCE_DAMAGE_AMOUNT_PER_TIMEOUT)

	call RemoveLocation(a)
	call RemoveLocation(b)
	call RemoveLocation(c)
	call RemoveLocation(d)
endfunction

function alce_init takes nothing returns nothing
	local conditionfunc f = null
	local trigger t = null

	set alce_head = 0

	set f = Condition(function alce_trig_filter)
	set t = spell_trig_init(f, function alce_trig_action)
endfunction
