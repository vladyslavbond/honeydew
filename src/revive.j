// jass_module(`SRC_REVIVE_J')
// jass_include(`SRC_USER_J', `src/user.j')
globals
	// src/revive.j fields
	constant real REVIVE_DURATION_SEC_MIN = 12.0
	constant real REVIVE_DURATION_SEC_MAX = 60.0
	constant real REVIVE_DURATION_SEC_BASE = 12.0
	constant real REVIVE_DURATION_SEC_COEF = 6.0

	constant integer REVIVE_PRICE_BASE = 170
	constant integer REVIVE_PRICE_COEF = 42

	integer array revive_player_count
	timer array revive_timer_list
	timerdialog array revive_dialog_list
	unit array revive_unit_list
endglobals

// src/revive.j functions

// Check if any user has hero revival pending.
function revive_ongoing_check takes nothing returns boolean
	local integer i = 0
	local integer q = 0

	set i = 0
	loop
		exitwhen i >= bj_MAX_PLAYERS
		set q = q + revive_player_count[i]
		set i = i + 1
	endloop
	return q > 0
endfunction

// Calculate the revival duration for the given hero unit.
function revive_duration_get takes unit u returns real
	local real duration = REVIVE_DURATION_SEC_MIN

	if null == u then
		set duration = 0.0
	elseif IsHeroUnitId(GetUnitTypeId(u)) and IsUnitType(u, UNIT_TYPE_HERO) then
		set duration = REVIVE_DURATION_SEC_BASE + (GetHeroLevel(u) - 1) * REVIVE_DURATION_SEC_COEF
	else
		set duration = REVIVE_DURATION_SEC_BASE + (GetUnitLevel(u) - 1) * REVIVE_DURATION_SEC_COEF
	endif

	return RMinBJ(RMaxBJ(REVIVE_DURATION_SEC_MIN, duration), REVIVE_DURATION_SEC_MAX)
endfunction

// Calculate the revival gold price for the given hero unit.
function revive_price_get takes unit u returns integer
	if null == u then
		return 0
	endif

	if IsHeroUnitId(GetUnitTypeId(u)) and IsUnitType(u, UNIT_TYPE_HERO) then
		return REVIVE_PRICE_BASE + (GetHeroLevel(u) - 1) * REVIVE_PRICE_COEF
	endif

	return 0
endfunction

// Instantly and without constraints or penalties
// revive given hero unit at the owner's start location.
function revive_unit takes unit u returns nothing
	local player p = null
	local integer start = -1
	local location loc = null

	if null == u then
		return
	endif

	set p = GetOwningPlayer(u)
	set start = GetPlayerStartLocation(p)
	set loc = Location(GetStartLocationX(start), GetStartLocationY(start) - 128.0)

	call ReviveHeroLoc(u, loc, true)
	call RemoveLocation(loc)
	if not IsUnitAliveBJ(u) then
		return
	endif
	call SetUnitFacing(u, bj_UNIT_FACING)
	if p == GetLocalPlayer() then
		call PanCameraTo(GetUnitX(u), GetUnitY(u))
		call SelectUnit(u, true)
	endif
endfunction

function revive_schedule_timer_callback takes nothing returns nothing
	local timer t = GetExpiredTimer()
	local unit u = null
	local integer i = 0
	local timerdialog g = null
	local integer k = bj_MAX_PLAYERS

	if null == t then
		return
	endif

	set i = 0
	loop
		exitwhen i >= JASS_MAX_ARRAY_SIZE or u != null or null == revive_timer_list[i]
		if t == revive_timer_list[i] then
			set u = revive_unit_list[i]
			set g = revive_dialog_list[i]
		endif
		set i = i + 1
	endloop
	call DestroyTimer(t)
	call DestroyTimerDialog(g)

	set k = GetPlayerId(GetOwningPlayer(u))
	set revive_player_count[k] = revive_player_count[k] - 1

	call revive_unit(u)	
endfunction

// Pay gold price for revival of the given hero unit,
// then delay effective revival for some duration.
function revive_schedule takes unit u returns timer
	local timer t = null
	local real duration = REVIVE_DURATION_SEC_MIN
	local timerdialog g = null
	local string label = null
	local integer i = 0
	local integer j = JASS_MAX_ARRAY_SIZE
	local integer k = bj_MAX_PLAYERS

	if null == u then
		return null
	endif

	if IsHeroUnitId(GetUnitTypeId(u)) and IsUnitType(u, UNIT_TYPE_HERO) then
		set t = CreateTimer()
		set duration = revive_duration_get(u)
		set duration = RMinBJ(RMaxBJ(REVIVE_DURATION_SEC_MIN, duration), REVIVE_DURATION_SEC_MAX)

		set g = CreateTimerDialog(t)
		set label = GetPlayerName(GetOwningPlayer(u)) + " " + GetUnitName(u)
		call TimerDialogSetTitle(g, label)
		call TimerDialogDisplay(g, true)

		call TimerStart(t, duration, false, function revive_schedule_timer_callback)

		set k = GetPlayerId(GetOwningPlayer(u))
		set revive_player_count[k] = revive_player_count[k] + 1

		set i = 0
		loop
			exitwhen i >= JASS_MAX_ARRAY_SIZE or (j < JASS_MAX_ARRAY_SIZE and j >= 0)
			if null == revive_timer_list[i] then
				set j = i
				set revive_timer_list[j] = t
				set revive_dialog_list[j] = g
				set revive_unit_list[j] = u
			endif
			set i = i + 1
		endloop
	endif

	return t
endfunction

// Check if owning player has the gold to revive given hero unit.
// If so, then schedule revival.
function revive_request takes unit u returns boolean
	local integer gold_available = 0
	local integer gold_price = 0
	local boolean hero_flag = false
	local player p = null
	local timer t = null

	if null == u then
		return false
	endif

	set hero_flag = IsHeroUnitId(GetUnitTypeId(u)) and IsUnitType(u, UNIT_TYPE_HERO)

	set p = GetOwningPlayer(u)
	set gold_available = GetPlayerState(p, PLAYER_STATE_RESOURCE_GOLD)
	set gold_price = revive_price_get(u)

	if hero_flag and gold_available >= gold_price then
		call SetPlayerState(p, PLAYER_STATE_RESOURCE_GOLD, gold_available - gold_price)
		set t = revive_schedule(u)
		return t != null
	endif

	return false
endfunction

function revive_retry_timer_callback takes nothing returns nothing
	local timer t = GetExpiredTimer()
	local unit u = null
	local boolean success = false
	local integer i = 0

	if null == t then
		return
	endif

	set i = 0
	loop
		exitwhen i >= JASS_MAX_ARRAY_SIZE or u != null or null == revive_timer_list[i]
		if t == revive_timer_list[i] then
			set u = revive_unit_list[i]
		endif
		set i = i + 1
	endloop

	set success = revive_request(u)	
	if success then
		call DestroyTimer(t)
	endif
endfunction

// Repeatedly request revival until the owning player has enough gold to schedule revival.
// There are more clever and optimized ways of doing this, but this way is also robust and obvious.
function revive_retry takes unit u returns nothing
	local timer t = null
	local integer i = 0
	local integer j = JASS_MAX_ARRAY_SIZE

	if u != null and IsHeroUnitId(GetUnitTypeId(u)) and IsUnitType(u, UNIT_TYPE_HERO) then
		set t = CreateTimer()

		set i = 0
		loop
			exitwhen i >= JASS_MAX_ARRAY_SIZE or (j < JASS_MAX_ARRAY_SIZE and j >= 0)
			if null == revive_timer_list[i] then
				set j = i
				set revive_timer_list[j] = t
				set revive_unit_list[j] = u
			endif
			set i = i + 1
		endloop

		call TimerStart(t, 2.0, true, function revive_retry_timer_callback)
	endif
endfunction

function revive_trig_action takes nothing returns nothing
	local unit u = null
	local boolean success = false

	set u = GetRevivableUnit()

	if null == u then
		return
	endif

	set success = revive_request(u)

	if not success then
		call revive_retry(u)
	endif
endfunction

// Set up mechanism to conditionally revive player heroes.
function revive_init takes nothing returns trigger
	local integer i = 0
	local trigger t = null
	local player p = null

	set t = CreateTrigger()

	set i = 0
	loop
		exitwhen i >= bj_MAX_PLAYERS
		set p = Player(i)
		set revive_player_count[i] = 0
		if user_player_is_user_check(p) then
			call TriggerRegisterPlayerUnitEvent(t, p, EVENT_PLAYER_HERO_REVIVABLE, null)
		endif
		set i = i + 1
	endloop
	call TriggerAddAction(t, function revive_trig_action)

	return t
endfunction
