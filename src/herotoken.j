// jass_module(`SRC_HEROTOKEN_J')
// jass_include(`SRC_USER_J', `src/user.j')
globals
	// src/herotoken.j fields
	constant integer HEROTOKEN_QUANTITY_MAX = 3
endglobals

// src/herotoken.j functions

// Every player will only have one hero of the same type.
function herotoken_unique_hero_trig_init takes nothing returns nothing
	call MeleeStartingHeroLimit()
	// TODO Add custom heroes to the list of limited units.
endfunction

// Live users at any time may buy or summon at most HEROTOKEN_QUANTITY_MAX hero units total across all users.
// function herotoken_init distributes these hero slots evenly among all present users.
function herotoken_hero_limit_init takes nothing returns nothing
	local integer hero_slot_available_quantity = HEROTOKEN_QUANTITY_MAX
	local integer user_quantity = 0
	local integer i = 0
	local integer q = 0
	local player p = null

	set user_quantity = user_quantity_get()
	set hero_slot_available_quantity = HEROTOKEN_QUANTITY_MAX
	if user_quantity > hero_slot_available_quantity then
		call BJDebugMsg("FIXME too many users and not enough hero slots available")
	endif

	loop
		exitwhen hero_slot_available_quantity <= 0 or user_quantity < 1
		set i = 0
		loop
			exitwhen i >= bj_MAX_PLAYERS
			set p = Player(i)
			if user_player_is_user_check(p) then
				set q = GetPlayerTechMaxAllowed(p, 'HERO') + 1
				// Set total permissible owned hero quantity for given player.
				call SetPlayerTechMaxAllowed(p, 'HERO', q)
				// Set free to summon hero quantity for given player.
            			call SetPlayerState(p, PLAYER_STATE_RESOURCE_HERO_TOKENS, q)
				set hero_slot_available_quantity = hero_slot_available_quantity - 1
			endif
			set i = i + 1
		endloop
	endloop
endfunction

function herotoken_init takes nothing returns nothing
	call herotoken_hero_limit_init()
	call herotoken_unique_hero_trig_init()
endfunction
