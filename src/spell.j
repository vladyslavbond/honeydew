// jass_module(`SRC_SPELL_J')

// Spell events will always trigger in this order.
// * channel (mandatory);
// * cast (optional);
// * effect (optional);
// * finish (optional);
// * endcast (mandatory);

// ---
// EVENT_PLAYER_UNIT_SPELL_CHANNEL
// ---
// Order to cast received.
// All calls to GetUnitCurrentOrder will return the same order identifier.
// Mana is not subtracted.
// GetUnitState will return mana _before_ subtraction.
// Channel event will always trigger.

// ---
// EVENT_PLAYER_UNIT_SPELL_CAST
// ---
// Mana will be subtracted.
// GetUnitState will return mana _before_ subtraction.
// Cast event will trigger _after_ "casting time" (Cast) expires.
// Cast event will trigger _before_ "follow through time" (DataA) expires.
// EffectArt will be rendered from this point.

// ---
// EVENT_PLAYER_UNIT_SPELL_EFFECT
// ---
// Mana will be subtracted.
// GetUnitState will return mana _before_ subtraction.
// Spell was not canceled and will take effect.
// Effect event will trigger _after_ "casting time" (Cast) expires.
// Effect event will trigger _before_ "follow through time" (DataA) expires.

// ---
// EVENT_PLAYER_UNIT_SPELL_FINISH
// ---
// Mana was subtracted.
// GetUnitState will return mana _after_ subtraction.
// Finish event will trigger given the spell finished gracefully uninterrupted,
// and "follow through time" duration expired completely.

// ---
// EVENT_PLAYER_UNIT_SPELL_ENDCAST
// ---
// Mana was subtracted.
// GetUnitState will return mana _after_ subtraction.
// Endcast event will always trigger.

// Filter checks ability id or maybe caster.
function spell_trig_init takes conditionfunc filter, code action returns trigger
	local trigger t = null
	local player p = null
	local integer i = 0

	if null == action then
		return null
	endif

	set t = CreateTrigger()
	set i = 0
	loop
		// bj_MAX_PLAYERS is max amount of users.
		// bj_MAX_PLAYER_SLOTS is max amount players, including neutral utility presets.
		exitwhen i >= bj_MAX_PLAYER_SLOTS
		set p = Player(i)
		call TriggerRegisterPlayerUnitEvent(t, p, EVENT_PLAYER_UNIT_SPELL_EFFECT, null)
		set i = i + 1
	endloop

	if filter != null then
		call TriggerAddCondition(t, filter)
	endif
	call TriggerAddAction(t, action)

	return t
endfunction
