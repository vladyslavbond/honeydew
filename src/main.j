// jass_module(`SRC_MAIN_J')
// jass_include(`SRC_USER_J', `src/user.j')
// jass_include(`SRC_ABLA_J', `src/abla.j')
// jass_include(`SRC_ADUM_J', `src/adum.j')
// jass_include(`SRC_ALCE_J', `src/alce.j')
// jass_include(`SRC_HEROTOKEN_J', `src/herotoken.j')
// jass_include(`SRC_DEFEAT_J', `src/defeat.j')
// jass_include(`SRC_VICTORY_J', `src/victory.j')
// jass_include(`SRC_REVIVE_J', `src/revive.j')
// jass_include(`SRC_ALTAR_J', `src/altar.j')

globals
	// src/main.j fields
	constant player PLAYER_DEMON = Player(3)
endglobals

// src/main.j functions

function init_demon takes player p returns unit
	local location loc = null
	local integer start = 0
	local unit u = null
	set start = GetPlayerStartLocation(p)
	set loc = GetStartLocationLoc(start)
	set u = CreateUnitAtLoc(p, 'Udre', loc, bj_UNIT_FACING)
	call SetUnitScale(u, 1.3, 1.3, 1.3)
	call SetHeroLevel(u, 10, false)
	call SetHeroStr(u, GetHeroStr(u, false) + 36, true)
	call SetHeroInt(u, GetHeroInt(u, false) + 12, true)
	call SetUnitUseFood(u, false)
	call SetPlayerState(p, PLAYER_STATE_RESOURCE_FOOD_CAP, 200)
	call RemoveLocation(loc)
	return u
endfunction

function init takes nothing returns nothing
	local unit demon = null

	// Spells
	call abla_init()
	call adum_init()
	call alce_init()

	call user_init()
	set demon = init_demon(PLAYER_DEMON)
	call herotoken_init()
	call altar_init()
	call revive_init()
	call defeat_init()
	call victory_init(demon)
	call ExecuteFunc("test_init")
endfunction

function main takes nothing returns nothing
	call SetCameraBounds( -7424.0 + GetCameraMargin(CAMERA_MARGIN_LEFT), -7680.0 + GetCameraMargin(CAMERA_MARGIN_BOTTOM), 7424.0 - GetCameraMargin(CAMERA_MARGIN_RIGHT), 7168.0 - GetCameraMargin(CAMERA_MARGIN_TOP), -7424.0 + GetCameraMargin(CAMERA_MARGIN_LEFT), 7168.0 - GetCameraMargin(CAMERA_MARGIN_TOP), 7424.0 - GetCameraMargin(CAMERA_MARGIN_RIGHT), -7680.0 + GetCameraMargin(CAMERA_MARGIN_BOTTOM) )
	call SetDayNightModels( "Environment\\DNC\\DNCLordaeron\\DNCLordaeronTerrain\\DNCLordaeronTerrain.mdl", "Environment\\DNC\\DNCLordaeron\\DNCLordaeronUnit\\DNCLordaeronUnit.mdl" )
	call NewSoundEnvironment( "Default" )
	call SetAmbientDaySound( "LordaeronWinterDay" )
	call SetAmbientNightSound( "LordaeronWinterNight" )
	call SetMapMusic( "Music", true, 0 )
	call InitBlizzard(  )
	call init()
endfunction
