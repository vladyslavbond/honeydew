// jass_module(`SRC_ADUM_J')
// jass_include(`SRC_SPELL_J', `src/spell.j')

// Spell Dampening Field.

// Create a circle of spell immunity at a target location. Every unit within
// the circle is granted the Spell Immunity passive ability. This spell may
// help enemies.

// When spell is cast, it creates an identity with all relevant properties.

// When adum_main_timer periodic timer expires, it iterates over every spell
// cast identity. First it clears adum_main_group. Then it re-populates the
// group.

// The group adum_main_group collects all units that are affected by any
// existing dampening field spell cast.

// Beside adum_main_timer callback, the above mentioned procedures of
// collecting and removing units from the group is done each time a dampening
// field spell effect is created or destroyed.

// When dampening field spell effect is created, a timer that is unique to the
// spell effect is attached to it. This timer will run the destructor when it
// expires and representes the spell effect's duration or lifespan.

// The computations are resource intensive. The hope is that the robustness is
// worth it. Units may die and be moved around in unexpected manner. The blunt
// iterator over time is expected to account for the most amount of corner
// cases.

globals

	// src/adum.j fields

	constant integer ADUM_ABILITY_ID = 'A001'
	constant integer ADUM_SPELL_IMMUNITY_ABILITY_ID = 'ACm2'

	constant real ADUM_RADIUS_BASE = 200.0
	constant real ADUM_RADIUS_COEF = 0.0
	constant real ADUM_RADIUS_MAX = 1024.0
	constant real ADUM_RADIUS_MIN = 32.0

	constant real ADUM_DURATION_SEC_BASE = 12.0
	constant real ADUM_DURATION_SEC_COEF = 0.0
	constant real ADUM_DURATION_SEC_MAX = 3600.0
	constant real ADUM_DURATION_SEC_MIN = 1.0

	constant string ADUM_SPLAT_FILE = "ReplaceableTextures\\Splats\\TeleportTarget.blp"
	constant integer ADUM_SPLAT_ENUM = 4

	constant real ADUM_MAIN_TIMER_TIMEOUT_SEC = 0.5

	integer array adum_ability_id
	unit array adum_caster
	location array adum_circle_center
	real array adum_circle_radius
	real array adum_duration_sec
	timer array adum_timer
	image array adum_decal
	effect array adum_area_effect

	integer adum_head = 0
	group adum_main_group = null
	timer adum_main_timer = null
endglobals

// src/adum.j functions

function adum_destroy takes integer i returns nothing
	local integer j = 0
	local integer p = 0
	local integer q = 0

	if i < 0 or i >= JASS_MAX_ARRAY_SIZE then
		return
	endif

	set j = adum_head

	if j < 0 or j >= JASS_MAX_ARRAY_SIZE then
		return
	endif

	call DestroyEffect(adum_area_effect[i])
	call DestroyTimer(adum_timer[i])
	call RemoveLocation(adum_circle_center[i])
	call DestroyImage(adum_decal[i])

	set p = i
	loop
		exitwhen p >= j or p >= JASS_MAX_ARRAY_SIZE

		set q = p + 1
		if q < JASS_MAX_ARRAY_SIZE then
			set adum_ability_id[p] = adum_ability_id[q]
			set adum_caster[p] = adum_caster[q]
			set adum_circle_center[p] = adum_circle_center[q]
			set adum_circle_radius[p] = adum_circle_radius[q]
			set adum_duration_sec[p] = adum_duration_sec[q]
			set adum_timer[p] = adum_timer[q]
			set adum_decal[p] = adum_decal[q]
			set adum_area_effect[p] = adum_area_effect[q]
		endif

		set p = p + 1
	endloop
endfunction

function adum_apply_group_filter takes nothing returns boolean
	local boolean flag = true
	local unit u = null

	set u = GetFilterUnit()
	if null == u then
		return false
	endif

	set flag = GetUnitState(u, UNIT_STATE_LIFE) > 0.0 and flag
	set flag = not IsUnitType(u, UNIT_TYPE_DEAD) and flag
	set flag = not IsUnitType(u, UNIT_TYPE_STRUCTURE) and flag

	return flag
endfunction

function adum_apply_group_callback takes nothing returns nothing
	local unit u = null

	set u = GetEnumUnit()
	if null == u then
		return
	endif

	if not IsUnitInGroup(u, adum_main_group) then
		call UnitAddAbility(u, ADUM_SPELL_IMMUNITY_ABILITY_ID)
		call GroupAddUnit(adum_main_group, u)
	endif
endfunction

function adum_apply takes location destination, real radius returns nothing
	local group g = null
	local filterfunc filter = null

	if null == destination then
		return
	endif

	set radius = RMinBJ(RMaxBJ(ADUM_RADIUS_MIN, radius), ADUM_RADIUS_MAX)

	if null == adum_main_group then
		set adum_main_group = CreateGroup()
	endif

	set g = CreateGroup()
	set filter = Filter(function adum_apply_group_filter)

	call GroupEnumUnitsInRangeOfLoc(g, destination, radius, filter)

	call ForGroup(g, function adum_apply_group_callback)

	call DestroyGroup(g)
	call DestroyFilter(filter)
endfunction

function adum_unit_check takes unit u returns boolean
	local integer i = 0
	local location o = null
	local real r = 0.0

	if null == u then
		return false
	endif

	set i = 0
	loop
		exitwhen i >= adum_head or i >= JASS_MAX_ARRAY_SIZE
		set o = adum_circle_center[i]
		set r = adum_circle_radius[i]
		if IsUnitInRangeLoc(u, o, r) then
			return true
		endif
		set i = i + 1
	endloop
	return false
endfunction

function adum_unapply_group_callback takes nothing returns nothing
	local unit u = null

	set u = GetEnumUnit()
	if null == u then
		return
	endif

	if not adum_unit_check(u) then
		call UnitRemoveAbility(u, ADUM_SPELL_IMMUNITY_ABILITY_ID)
	endif
endfunction

function adum_unapply takes group g returns nothing
	if null == g then
		return
	endif
	call ForGroup(g, function adum_unapply_group_callback)
	call GroupClear(g)
endfunction

function adum_timer_callback takes nothing returns nothing
	local timer t = null
	local integer i = 0
	local integer j = 0

	set i = 0
	set j = adum_head
	set t = GetExpiredTimer()
	loop
		exitwhen i >= j
		if t == adum_timer[i] then
			call adum_destroy(i)

			set adum_head = adum_head - 1
		endif
		set i = i + 1
	endloop

	if adum_main_group != null then
		call adum_unapply(adum_main_group)
	endif

	if adum_head <= 0 then
		call PauseTimer(adum_main_timer)
	endif

	call DestroyTimer(t)
endfunction


function adum_main_timer_callback takes nothing returns nothing
	local unit c = null
	local integer i = 0
	local integer j = 0

	call adum_unapply(adum_main_group)

	set i = 0
	set j = adum_head
	loop
		exitwhen i >= j

		call adum_apply(adum_circle_center[i], adum_circle_radius[i])

		// Given the spell was cast by a unit,
		// given the unit is dead,
		// destroy the spell effect.
		set c = adum_caster[i]
		if c != null then
			if GetUnitState(c, UNIT_STATE_LIFE) < 1.0 or IsUnitType(c, UNIT_TYPE_DEAD) then
				call adum_destroy(i)

				set adum_head = adum_head - 1
			endif
		endif

		set i = i + 1
	endloop
endfunction

function adum_cast takes integer ability_id, unit caster, location destination, real radius, real duration_sec returns nothing
	local image splat = null
	local integer i = 0
	local real s = 0.0
	local real x = 0.0
	local real y = 0.0
	local real z = 0.0

	if null == destination then
		return
	endif

	set radius = RMinBJ(RMaxBJ(ADUM_RADIUS_MIN, radius), ADUM_RADIUS_MAX)

	set duration_sec = RMinBJ(RMaxBJ(ADUM_DURATION_SEC_MIN, duration_sec), ADUM_DURATION_SEC_MAX)

	if adum_head < 0 or adum_head >= JASS_MAX_ARRAY_SIZE then
		return
	endif

	set i = adum_head

	set adum_ability_id[i] = ability_id
	set adum_caster[i] = caster
	set x = GetLocationX(destination)
	set y = GetLocationY(destination)
	set adum_circle_center[i] = Location(x, y)
	set adum_circle_radius[i] = radius
	set adum_duration_sec[i] = duration_sec
	set adum_timer[i] = CreateTimer()
	call TimerStart(adum_timer[i], duration_sec, false, function adum_timer_callback)

	set s = radius * 2.0
	set z = 24.0
	set splat = CreateImage(ADUM_SPLAT_FILE, s, s, s, x - radius, y - radius, z, 0, 0, 0, ADUM_SPLAT_ENUM)
	// It is mandatory to call SetImageRenderAlways after CreateImage
	call SetImageRenderAlways(splat, true)
	call ShowImage(splat, true)
	set adum_decal[i] = splat

	set adum_area_effect[i] = AddSpellEffectByIdLoc(adum_ability_id[i], EFFECT_TYPE_AREA_EFFECT, adum_circle_center[i])

	set adum_head = adum_head + 1
	if adum_head < 0 or adum_head >= JASS_MAX_ARRAY_SIZE then
		set adum_head = 0
	endif

	call adum_apply(adum_circle_center[i], adum_circle_radius[i])

	call TimerStart(adum_main_timer, ADUM_MAIN_TIMER_TIMEOUT_SEC, true, function adum_main_timer_callback)
endfunction

function adum_trig_filter takes nothing returns boolean
	return ADUM_ABILITY_ID == GetSpellAbilityId()
endfunction

function adum_trig_action takes nothing returns nothing
	local integer ability_id = 0
	local integer ability_level = 0
	local unit caster = null
	local location destination = null
	local real radius = 0.0
	local real duration_sec = 0.0

	set ability_id = GetSpellAbilityId()
	set ability_level = GetUnitAbilityLevel(caster, ability_id)
	set caster = GetSpellAbilityUnit()
	set destination = GetSpellTargetLoc()

	set radius = ADUM_RADIUS_BASE + ADUM_RADIUS_COEF * ability_level
	set radius = RMinBJ(RMaxBJ(ADUM_RADIUS_MIN, radius), ADUM_RADIUS_MAX)

	set duration_sec = ADUM_DURATION_SEC_BASE + ADUM_DURATION_SEC_COEF * ability_level
	set duration_sec = RMinBJ(RMaxBJ(ADUM_DURATION_SEC_MIN, duration_sec), ADUM_DURATION_SEC_MAX)

	call adum_cast(ability_id, caster, destination, radius, duration_sec)

	call RemoveLocation(destination)
endfunction

function adum_init takes nothing returns nothing
	local trigger trig = null
	local conditionfunc filter = null

	set adum_head = 0
	set adum_main_group = CreateGroup()
	set adum_main_timer = CreateTimer()
	call TimerStart(adum_main_timer,  ADUM_MAIN_TIMER_TIMEOUT_SEC, true, function adum_main_timer_callback)
	call PauseTimer(adum_main_timer)

	set filter = Condition(function adum_trig_filter)
	set trig = spell_trig_init(filter, function adum_trig_action)
endfunction
