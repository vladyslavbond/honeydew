// jass_module(`SRC_VICTORY_J')
// jass_include(`SRC_USER_J', `src/user.j')
globals

	// src/victory.j fields

endglobals

// src/victory.j functions

function victory_player takes player p returns nothing
	if null == p then
		return
	endif

	call CustomVictoryBJ(p, true, true)
endfunction

function victory_force_callback takes nothing returns nothing
	call victory_player(GetEnumPlayer())
endfunction

function victory_quest_complete_timer_callback takes nothing returns nothing
	local timer t = null
	local force f = null

	set t = GetExpiredTimer()
	call DestroyTimer(t)

	set f = user_force_get()
	call ForForce(f, function victory_force_callback)
endfunction

function victory_quest_complete_msg takes nothing returns nothing
	local force f = null

	set f = user_force_get()

	call QuestMessageBJ(f, bj_QUESTMESSAGE_COMPLETED, "|cffffcc00MAIN QUEST COMPLETED|r\nDemon")
endfunction

function victory takes nothing returns nothing
	local timer t = null
	call victory_quest_complete_msg()
	set t = CreateTimer()
	call TimerStart(t, bj_QUEUE_DELAY_QUEST, false, function victory_quest_complete_timer_callback)
	// TODO add victory cinematic
endfunction

function victory_trig_action takes nothing returns nothing
	local trigger t = null

	set t = GetTriggeringTrigger()
	call DisableTrigger(t)

	call victory()
endfunction

function victory_quest_discover_msg takes nothing returns nothing
	local force f = null
	local string fluff = null

	set f = user_force_get()

	set fluff = "|cffffcc00MAIN QUEST|r\nDemon\n- defeat demon villain"
	call QuestMessageBJ(f, bj_QUESTMESSAGE_DISCOVERED, fluff)
endfunction

function victory_quest_discover_timer_callback takes nothing returns nothing
	local timer t = null

	set t = GetExpiredTimer()
	call DestroyTimer(t)

	call victory_quest_discover_msg()
endfunction

function victory_quest_init takes nothing returns quest
	local quest q = null
	local questitem m = null
	local string fluff = null

	set q = CreateQuest()

	// FIXME Globalize victory quest fluff

	call QuestSetTitle(q, "Demon")

	set fluff = "Deep in enchanted forests, there exists a sacred lake. "
	set fluff = fluff + "A cunning demon discovered it and attempts to corrupt it. "
	set fluff = fluff + "Three heroes are summoned to protect the sanctuary and defeat the demon."

	call QuestSetDescription(q, fluff)

	call QuestSetIconPath(q, "ReplaceableTextures\\CommandButtons\\BTNHeroDreadLord.blp")

	call QuestSetRequired(q, true)
	call QuestSetCompleted(q, false)
	call QuestSetDiscovered(q, true)
	call QuestSetFailed(q, false)
	call QuestSetEnabled(q, true)

	set m = QuestCreateItem(q)
	call QuestItemSetDescription(m, "defeat demon villain")
	
	return q
endfunction

function victory_trig_init takes unit demon returns trigger
	local trigger t = null

	if null == demon then
		call BJDebugMsg("src/victory.j: could not initialize victory condition: boss unit not found")
	endif

	set t = CreateTrigger()
	call TriggerRegisterUnitEvent(t, demon, EVENT_UNIT_DEATH)
	call TriggerAddAction(t, function victory_trig_action)
	return t
endfunction

function victory_init takes unit demon returns nothing
	local timer t = null
	call victory_quest_init()
	call victory_trig_init(demon)
	// TODO Move main quest message somewhere else
	set t = CreateTimer()
	call TimerStart(t, bj_QUEUE_DELAY_QUEST, false, function victory_quest_discover_timer_callback)
endfunction
