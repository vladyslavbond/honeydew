// jass_module(`SRC_CONFIG_J')

globals

	// src/config.j fields

	constant integer TEAM_ENEMY = 1
	constant integer TEAM_USER = 0
	constant integer USER_QUANTITY_MAX = 3
endglobals

// src/config.j functions

function config_slot_user takes integer n returns nothing
	local integer i = 0
	local player p = null
	local player e = null

	if not (n >= 0 and n < bj_MAX_PLAYER_SLOTS) then
		return
	endif

	set p = Player(n)

	call SetPlayerStartLocation(p, n)
	call SetPlayerColor(p, ConvertPlayerColor(n))
	call SetPlayerRacePreference(p, RACE_PREF_RANDOM)
	call SetPlayerRaceSelectable(p, true)
	call SetPlayerController(p, MAP_CONTROL_USER)
	call SetPlayerTeam(p, TEAM_USER)

	set i = 0
	loop
		exitwhen i >= USER_QUANTITY_MAX

		set e = Player(i)
		call SetPlayerAlliance(p, e, ALLIANCE_PASSIVE, true)
		call SetPlayerAlliance(p, e, ALLIANCE_HELP_REQUEST, true)
		call SetPlayerAlliance(p, e, ALLIANCE_HELP_RESPONSE, true)
		call SetPlayerAlliance(p, e, ALLIANCE_SHARED_XP, true)
		call SetPlayerAlliance(p, e, ALLIANCE_SHARED_VISION, true)
		set i = i + 1
	endloop

	call SetPlayerOnScoreScreen(p, true)
endfunction

function config_slot_computer takes integer n returns nothing
	local player p = null

	if not (n >= 0 and n < bj_MAX_PLAYER_SLOTS) then
		return
	endif

	set p = Player(n)

	call SetPlayerStartLocation(p, n)
	call ForcePlayerStartLocation(p, n)
	call SetPlayerColor(p, PLAYER_COLOR_BLUE)
	call SetPlayerRacePreference(p, RACE_PREF_UNDEAD)
	call SetPlayerRaceSelectable(p, false)
	call SetPlayerController(p, MAP_CONTROL_COMPUTER)
	call SetPlayerTeam(p, TEAM_ENEMY)
	call SetPlayerName(p, "Demon")
	call SetPlayerOnScoreScreen(p, true)
endfunction

function config_every_start takes nothing returns nothing
	local location origin_loc = null
	local location loc = null
	local integer i = 0
	local real x = 0.0
	local real y = 0.0

	set origin_loc = Location(0.0, 0.0)

	set i = 0
	loop
		exitwhen i >= USER_QUANTITY_MAX
		set x = GetLocationX(origin_loc) + 128.0 * (i + 1)
		set y = GetLocationY(origin_loc) - 512.0 * 4.0
		set loc = Location(x, y)
		call DefineStartLocationLoc(i, loc)
		call RemoveLocation(loc)
		set i = i + 1
	endloop

	set x = GetLocationX(origin_loc)
	set y = GetLocationY(origin_loc) + 512.0 * 4.0
	set loc = Location(x, y)
	call DefineStartLocationLoc(3, origin_loc)
	call RemoveLocation(loc)

	call RemoveLocation(origin_loc)
endfunction

function config_every_slot takes nothing returns nothing
	local integer i = 0

	loop
		exitwhen i >= USER_QUANTITY_MAX
		call config_slot_user(i)
		set i = i + 1
	endloop

	call config_slot_computer(3)
endfunction

function config takes nothing returns nothing
	call SetMapName( "TRIGSTR_001" )
	call SetMapDescription( "TRIGSTR_003" )
	call SetTeams(2)
	call SetPlayers(USER_QUANTITY_MAX + 1)
	call SetGamePlacement(MAP_PLACEMENT_TEAMS_TOGETHER)

	call SetGameTypeSupported(GAME_TYPE_USE_MAP_SETTINGS, true)

	call SetMapFlag(MAP_LOCK_ALLIANCE_CHANGES, true)
	call SetMapFlag(MAP_RESOURCE_TRADING_ALLIES_ONLY, true)
	call SetGamePlacement(MAP_PLACEMENT_FIXED)

	call config_every_start()
	call config_every_slot()
endfunction

