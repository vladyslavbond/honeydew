// jass_module(`SRC_ALTAR_J')
// jass_include(`SRC_USER_J', `src/user.j')
globals
	// src/altar.j fields

	constant integer ALTAR_ORDER_ID_CANCEL = 851976

	unit array altar_player_structure
endglobals

// src/altar.j functions

function altar_train_trig_filter takes nothing returns boolean
	local integer utype = GetTrainedUnitType()
	return utype != 0 and IsHeroUnitId(utype)
endfunction

// Use arcane magic to skip hero summoning process.
function altar_train_trig_action takes nothing returns nothing
	local integer utype = GetTrainedUnitType()
	local unit altar = GetTriggerUnit()
	local player p = GetOwningPlayer(altar)
	local unit u = null
	local effect e = null
	local location loc = Location(GetUnitX(altar), GetUnitY(altar) - 128.0)
	local integer g = 0
	local integer b = 0

	// Structure starts to produce a unit.
	// Create a unit of the same type before the completion.
	set u = CreateUnitAtLoc(p, utype, loc, bj_UNIT_FACING)
	call RemoveLocation(loc)
	if null == u then
		return
	endif
	call SetUnitUseFood(u, false)
	call SetUnitState(u, UNIT_STATE_MANA, GetUnitState(u, UNIT_STATE_MAX_MANA))

	// Remember how much resources was invested.
	set g = GetPlayerState(p, PLAYER_STATE_RESOURCE_GOLD)
	set b = GetPlayerState(p, PLAYER_STATE_RESOURCE_LUMBER)
	// Cancel the production.
	call IssueImmediateOrderById(altar, ALTAR_ORDER_ID_CANCEL)
	// After successful cancelation, the resources invested will be refunded.
	// Revert the resources to previous state,
	// to give the impression that they were consumed normally.
	call SetPlayerState(p, PLAYER_STATE_RESOURCE_GOLD, g)
	call SetPlayerState(p, PLAYER_STATE_RESOURCE_LUMBER, b)

	// Only one instance of a hero type is allowed per player.
	call SetPlayerTechMaxAllowed(p, utype, 1)

	set e = AddSpellEffectTargetById('Aawa', EFFECT_TYPE_TARGET, u, "origin")
	call DestroyEffect(e)
endfunction

function altar_train_trig_init takes nothing returns trigger
	local trigger t = null

	set t = CreateTrigger()
	call TriggerAddAction(t, function altar_train_trig_action)
	call TriggerAddCondition(t, Condition(function altar_train_trig_filter))
	call TriggerRegisterPlayerUnitEvent(t, Player(0), EVENT_PLAYER_UNIT_ISSUED_ORDER, null)

	return t
endfunction

function altar_player_altar_type_get takes player p returns integer
	return 'h000'
endfunction

function altar_player_altar_unit_get takes player p returns unit
	if null == p then
		return null
	endif

	return altar_player_structure[GetPlayerId(p)]
endfunction

function altar_player_structure_init takes player p returns unit
	local unit u = null
	local integer utype = 0
	local location loc = null

	if null == p then
		return null
	endif

	set u = altar_player_structure[GetPlayerId(p)]

	if u != null then
		return u
	endif

	set utype = altar_player_altar_type_get(p)
	set loc = GetStartLocationLoc(GetPlayerStartLocation(p))
	set u = CreateUnitAtLoc(p, utype, loc, bj_UNIT_FACING)

	call SetUnitInvulnerable(u, true)
	// Remove rally button to fit more hero buttons on the panel.
	call UnitRemoveAbility(u, 'ARal')

	if p == GetLocalPlayer() then
		call ClearSelection()
		call SelectUnit(u, true)
	endif

	call RemoveLocation(loc)

	set altar_player_structure[GetPlayerId(p)] = u

	return u
endfunction

function altar_structure_init takes nothing returns nothing
	local trigger train_trig = null
	local integer i = 0
	local unit altar = null
	local player p = null

	set train_trig = altar_train_trig_init()
	set i = 0
	loop
		exitwhen i >= bj_MAX_PLAYERS
		set p = Player(i)
		if user_player_is_user_check(p) then
			set altar = altar_player_structure_init(p)
			if altar != null and train_trig != null then
				call TriggerRegisterUnitEvent(train_trig, altar, EVENT_UNIT_TRAIN_START)
			endif
		endif
		set i = i + 1
	endloop
endfunction

function altar_init takes nothing returns nothing
	call altar_structure_init()
endfunction
