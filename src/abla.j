// jass_module(`SRC_ABLA_J')
// jass_include(`SRC_SPELL_J', `src/spell.j')
// jass_include(`SRC_ATUN_J', `src/atun.j')

// Spell Arcane blast.
// Deal damage to units in target area.
// Given unit that is immune to magic, heal the unit instead.
// This spell may heal enemies.
// This spell may harm allies.

globals

	// src/abla.j fields

	constant integer ABLA_ABILITY_ID = 'A000'

	constant integer ABLA_GROUP_LIMIT = 12
	constant real ABLA_RADIUS_MAX = 8192.0
	constant real ABLA_RADIUS_MIN = 64.0
	constant real ABLA_DAMAGE_AMOUNT_MAX = 100000.0
	constant real ABLA_DAMAGE_AMOUNT_MIN = 0.0
	constant real ABLA_HEAL_AMOUNT_MAX = 100000.0
	constant real ABLA_HEAL_AMOUNT_MIN = 0.0

	integer abla_super_abilityid = 0
	real abla_super_damage = 0.0
	real abla_super_heal = 0.0
	unit abla_super_caster = null

	effect array abla_channel_effect
	unit array abla_channel_unit
endglobals

// src/abla.j functions

function abla_harm_check takes unit u returns boolean
	local boolean flag = true
	//local player p = null

	if null == u then
		return false
	endif

	set flag = not IsUnitType(u, UNIT_TYPE_DEAD) and flag
	set flag = GetUnitState(u, UNIT_STATE_LIFE) > 0 and flag
	set flag = not IsUnitType(u, UNIT_TYPE_STRUCTURE) and flag
	set flag = not IsUnitType(u, UNIT_TYPE_MAGIC_IMMUNE) and flag
	// TODO Consider making Arcane Blast to never harm allies, but allow it to heal enemies.
	//set p = GetOwningPlayer(abla_super_caster)
	//set flag = IsUnitEnemy(u, p) and flag

	return flag
endfunction

function abla_heal_check takes unit u returns boolean
	local boolean flag = true

	if null == u then
		return false
	endif

	set flag = not IsUnitType(u, UNIT_TYPE_DEAD) and flag
	set flag = GetUnitState(u, UNIT_STATE_LIFE) > 0 and flag
	set flag = not IsUnitType(u, UNIT_TYPE_STRUCTURE) and flag
	set flag = IsUnitType(u, UNIT_TYPE_MAGIC_IMMUNE) and flag

	return flag
endfunction

function abla_harm_group_filter takes nothing returns boolean
	local unit u = GetFilterUnit()
	return abla_harm_check(u) and not abla_heal_check(u)
endfunction

function abla_heal_group_filter takes nothing returns boolean
	local unit u = GetFilterUnit()
	return abla_heal_check(u) and not abla_harm_check(u)
endfunction

function abla_harm_group_callback takes nothing returns nothing
	local integer abilityid = 0
	local unit caster = null
	local unit u = null
	local real damage = 0.0
	local boolean attack = true
	local boolean ranged = true
	local effect e = null
	local real m = 0.0

	set u = GetEnumUnit()
	if null == u then
		return
	endif

	set caster = abla_super_caster
	set damage = RMinBJ(RMaxBJ(ABLA_DAMAGE_AMOUNT_MIN, abla_super_damage), ABLA_DAMAGE_AMOUNT_MAX)

	// Note that this damage is affected by victim's armour type.
	call UnitDamageTarget(caster, u, damage, attack, ranged, ATTACK_TYPE_MAGIC, DAMAGE_TYPE_MAGIC, WEAPON_TYPE_WHOKNOWS)

	set abilityid = abla_super_abilityid
	set e = AddSpellEffectTargetById(abilityid, EFFECT_TYPE_TARGET, u, "origin")
	call DestroyEffect(e)

	set m = atun_mana_restore_amount_get(caster, u)
	call atun_mana_restore(caster, m)
endfunction

function abla_heal_group_callback takes nothing returns nothing
	local unit caster = null
	local unit u = null
	local real heal = 0.0
	local real a = 0.0
	local real b = 0.0
	local effect e = null

	set u = GetEnumUnit()
	if null == u then
		return
	endif

	set heal = RMinBJ(RMaxBJ(ABLA_HEAL_AMOUNT_MIN, abla_super_heal), ABLA_HEAL_AMOUNT_MAX)
	set a = GetUnitState(u, UNIT_STATE_LIFE)
	call SetUnitState(u, UNIT_STATE_LIFE, a + heal)
	set b = GetUnitState(u, UNIT_STATE_LIFE)


	// Only show healing graphics when the amount of health effectively changed.
	if b > a then
		// Only use effects that have a single one animation, that is guaranteed to play.
		set e = AddSpellEffectTargetById('BUav', EFFECT_TYPE_SPECIAL, u, "origin")
		call DestroyEffect(e)
	endif
endfunction

function abla_apply takes integer abilityid, unit caster, location destination, real radius, real damage, real healing returns nothing
	local group g = null
	local filterfunc filter = null
	local integer limit = 1
	local effect e = null

	if null == destination then
		return
	endif

	set destination = Location(GetLocationX(destination), GetLocationY(destination))

	set radius = RMinBJ(RMaxBJ(ABLA_RADIUS_MIN, radius), ABLA_RADIUS_MAX)
	set damage = RMinBJ(RMaxBJ(ABLA_DAMAGE_AMOUNT_MIN, damage), ABLA_DAMAGE_AMOUNT_MAX)
	set healing = RMinBJ(RMaxBJ(ABLA_HEAL_AMOUNT_MIN, healing), ABLA_HEAL_AMOUNT_MAX)
	set limit = ABLA_GROUP_LIMIT

	// Implicit arguments (upvalues) for group callbacks.
	set abla_super_abilityid = abilityid
	set abla_super_caster = caster
	set abla_super_damage = damage
	set abla_super_heal = healing

	set g = CreateGroup()

	call GroupClear(g)
	set filter = Filter(function abla_harm_group_filter)
	call GroupEnumUnitsInRangeOfLocCounted(g, destination, radius, filter, limit)
	call ForGroup(g, function abla_harm_group_callback)
	call DestroyFilter(filter)

	call GroupClear(g)
	set filter = Filter(function abla_heal_group_filter)
	call GroupEnumUnitsInRangeOfLocCounted(g, destination, radius, filter, limit)
	call ForGroup(g, function abla_heal_group_callback)
	call DestroyFilter(filter)

	set abla_super_abilityid = 0
	set abla_super_caster = null
	set abla_super_damage = 0.0
	set abla_super_heal = 0.0
	call DestroyGroup(g)

	set e = AddSpellEffectByIdLoc(abilityid, EFFECT_TYPE_SPECIAL, destination)
	call DestroyEffect(e)

	call RemoveLocation(destination)
endfunction

function abla_channel_effect_create takes integer ability_id, unit caster, location target returns effect
	local effect e = null
	local integer i = 0
	local boolean flag = false

	if null == caster then
		return null
	endif

	if null == target then
		return null
	endif

	set i = 0
	loop
		exitwhen i >= JASS_MAX_ARRAY_SIZE or flag
		if null == abla_channel_unit[i] then
			set abla_channel_unit[i] = caster
			call DestroyEffect(abla_channel_effect[i])
			set e = AddSpellEffectByIdLoc(ability_id, EFFECT_TYPE_AREA_EFFECT, target)
			set abla_channel_effect[i] = e
			set flag = true
		endif
		set i = i + 1
	endloop

	return e
endfunction

function abla_channel_effect_destroy takes unit caster returns nothing
	local integer i = 0
	local integer j = 0
	local boolean flag = false

	set i = 0
	loop
		exitwhen i >= JASS_MAX_ARRAY_SIZE or flag
		set flag = abla_channel_unit[i] == caster or flag
		if flag then
			call DestroyEffect(abla_channel_effect[i])
			set abla_channel_unit[i] = null
		endif
		set i = i + 1
	endloop
endfunction

function abla_trig_filter takes nothing returns boolean
	return ABLA_ABILITY_ID == GetSpellAbilityId()
endfunction

function abla_channel_action takes nothing returns nothing
	local integer abilityid = GetSpellAbilityId()
	local unit caster = GetSpellAbilityUnit()
	local effect e = null
	local location loc = GetSpellTargetLoc()
	set e = abla_channel_effect_create(abilityid, caster, loc)
	call RemoveLocation(loc)
endfunction

function abla_endcast_action takes nothing returns nothing
	local unit caster = GetSpellAbilityUnit()
	// The channel effect destructor must be triggered by SPELL_ENDCAST event.
	// If it's called by the same action as for example SPELL_EFFECT, runtime errors occurr.
	// The exact cause is unclear and the investigation isn't worth it at the time.
	call abla_channel_effect_destroy(caster)
endfunction

function abla_effect_action takes nothing returns nothing
	local integer abilityid = 0
	local unit caster = GetSpellAbilityUnit()
	local location destination = null
	local real area = 0.0
	local real damage = 0.0
	local real healing = 0.0
	local integer level = 0

	set abilityid = GetSpellAbilityId()
	set caster = GetSpellAbilityUnit()
	set level = GetUnitAbilityLevel(caster, abilityid)
	set destination = GetSpellTargetLoc()
	set area = 128.0
	set damage = 50.0 + 50.0 * I2R(level)
	set healing = 50.0 * I2R(level)

	call abla_apply(abilityid, caster, destination, area, damage, healing)

	call RemoveLocation(destination)
endfunction

function abla_init takes nothing returns nothing
	local conditionfunc filter = Condition(function abla_trig_filter)
	local trigger t0 = null
	local trigger t2 = null
	local trigger t4 = null
	local player p = null
	local integer i = 0

	set t0 = CreateTrigger()
	set t2 = CreateTrigger()
	set t4 = CreateTrigger()
	set i = 0
	loop
		// bj_MAX_PLAYERS is max amount of users.
		// bj_MAX_PLAYER_SLOTS is max amount players, including neutral utility presets.
		exitwhen i >= bj_MAX_PLAYER_SLOTS
		set p = Player(i)
		call TriggerRegisterPlayerUnitEvent(t0, p, EVENT_PLAYER_UNIT_SPELL_CHANNEL, null)
		call TriggerRegisterPlayerUnitEvent(t2, p, EVENT_PLAYER_UNIT_SPELL_EFFECT, null)
		call TriggerRegisterPlayerUnitEvent(t4, p, EVENT_PLAYER_UNIT_SPELL_ENDCAST, null)
		set i = i + 1
	endloop

	if filter != null then
		call TriggerAddCondition(t0, filter)
		call TriggerAddCondition(t2, filter)
		call TriggerAddCondition(t4, filter)
	endif
	call TriggerAddAction(t0, function abla_channel_action)
	call TriggerAddAction(t2, function abla_effect_action)
	call TriggerAddAction(t4, function abla_endcast_action)
endfunction
