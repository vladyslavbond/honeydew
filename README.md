# honeydew

This is a custom scenario for an old computer game WarCraft 3: The Frozen
Throne, made by fan community of modders.

The only supported version of the game is 1.27.

## Players

Players should only be interested in the map archive file, recognizeable by the
file extension `*.w3x`. This repository contains scripts that are used to
produce the complete map archive file ready to be played. These scripts
themselves are not needed at runtime during play.

The complete scenario that is ready to be played or tested should be found
naturally on fan websites. This repository and instructions it contains are
aimed at developers first.

## Developers

TODO add detailed build instructions

Instructions on how to build the project are contained in `doc/build.md` text
file. Beside the original game itself (patch 1.27), required tools include:
`git`, `pjass`, `m4`, `smpq`, `make`.

Source scripts for custom spells and other Jass utilities are under `src/`
directory. Most of these may be easily exported to other projects, without
understanding the whole build process.

## Copying

Everyone is free to use, modify or share this project. Text file `COPYING.txt`
contains more details on the mode in which this project is shared.

## Authors

Text file `AUTHORS.txt` contains list of contributions that were made
explicitly for this project alone. Tools and experience employed are outside of
it's scope, and are tracked entirely separately in their respective projects.
