// jass_module(`TEST_TEST_J')
// jass_include(`SRC_ABLA_J', `src/abla.j')
// jass_include(`SRC_ADUM_J', `src/adum.j')
// jass_include(`SRC_ATUN_J', `src/atun.j')

globals

	// test/test.j fields

	constant integer TEST_USER_ID = 0
	constant integer TEST_ARCANE_SORCERER_UNIT_TYPE_ID = 'H001'
endglobals

// test/test.j functions

function test_enemy_pool_get takes nothing returns unitpool
	local unitpool pool = null

	set pool = CreateUnitPool()

	call UnitPoolAddUnitType(pool, 'earc', 50.0)
	call UnitPoolAddUnitType(pool, 'nwat', 40.0)
	call UnitPoolAddUnitType(pool, 'edoc', 25.0)
	call UnitPoolAddUnitType(pool, 'edry', 25.0)
	call UnitPoolAddUnitType(pool, 'ehip', 25.0)

	return pool
endfunction

function test_enemy_init takes location o returns nothing
	local unitpool pool = null
	local integer i = 0
	local integer q = 12
	local player p = null
	local real x = 0.0
	local real y = 0.0
	local unit u = null

	if o == null then
		return
	endif

	set pool = test_enemy_pool_get()
	set p = Player(3)
	set x = GetLocationX(o)
	set y = GetLocationY(o)

	loop
		exitwhen i >= q
		set u = PlaceRandomUnit(pool, p, x, y, bj_UNIT_FACING)
		call UnitShareVision(u, Player(TEST_USER_ID), true)
		set i = i + 1
	endloop

	call DestroyUnitPool(pool)
endfunction

function test_init takes nothing returns nothing
	local player p = null
	local integer start = 0
	local location o = null
	local integer utype = 0
	local unit h = null
	local integer hq = 3

	set p = Player(TEST_USER_ID)

	set start = GetPlayerStartLocation(p)
	set o = GetStartLocationLoc(start)

	loop
		exitwhen hq <= 0

		set utype = TEST_ARCANE_SORCERER_UNIT_TYPE_ID
		set h = CreateUnitAtLoc(p, utype, o, bj_UNIT_FACING)
		call SetHeroLevel(h, 10, false)
		call SetUnitState(h, UNIT_STATE_MANA, GetUnitState(h, UNIT_STATE_MAX_MANA))
		call SetUnitUseFood(h, false)

		if p == GetLocalPlayer() then
			call ClearSelection()
			call SelectUnit(h, true)
		endif

		set hq = hq - 1
	endloop

	call MoveLocation(o, GetStartLocationX(start) - 512.0 * 3, GetStartLocationY(start) - 512.0 * 3)
	call test_enemy_init(o)

	call RemoveLocation(o)
endfunction
