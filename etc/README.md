# Blizzard Jass2 scripts

This directory should contain the following Blizzard script files:
  - common.j
  - Blizzard.j
  - common.ai

These files are distributed with the game and contain declarations of
funcctions and variables for Jass2 scripting language, that is available to
users at runtime.

These files are duplicated in the project's repository for several practical
reasons.

When the project is built, 'pjass' parses all the project specific scripts and
checks them for validtity before the game is even run or the campaign packaged.
Files with native declarations must be included in the tool's path for it to
work. Hence why these files must be readily present during development.

Additionally, these files specifically are used to decisively test the project
for validity against specific game version (1.27), from which these native
declarations were obtained from. If the project should be ported to some other
game version, these files must be upgraded to appropriate version.

'pjass' is a free community tool that is used to parse Jass2 scripts. Since
game version >1.31, 'pjass' should be included in the official game
distribution. Note that 'pjass' is easily built for GNU/Linux natively from
source.
